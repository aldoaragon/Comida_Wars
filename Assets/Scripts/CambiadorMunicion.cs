﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiadorMunicion : MonoBehaviour {

	public Sprite azul;
	public Sprite picos;
	public SpriteRenderer spriterenderer;
	//private GameObject gameObjetcTest;
	// Use this for initialization
	public void MunicionAzul ()
	{
		spriterenderer = GameObject.FindWithTag("Player").GetComponent<SpriteRenderer> ();

		spriterenderer.sprite = azul;
	} 

	public void MunicionPicos ()
	{
		spriterenderer = GameObject.FindWithTag("Player").GetComponent<SpriteRenderer> ();
		spriterenderer.sprite = picos;
	} 

}
