﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	public NuevoProyectil nuevoProyectil;
	public Transform spawnPoint;
	public GameObject prefab;
	public Rigidbody2D rb;
	public float releaseTime = 0.15f;
	public float tiempoVida = 6f;
    public string physiclayer;

	private bool destruir = false;

	private bool isPressed = false;

	// Use this for initialization
	void Start () {

		nuevoProyectil = GameObject.Find("spawn point").GetComponent<NuevoProyectil> ();
        nuevoProyectil.gameObject.layer = LayerMask.NameToLayer(physiclayer);



    }
	
	// Update is called once per frame
	void Update ()
	{

		if (isPressed) {

			//la posición del mouse se da en pixeles , la posición de unity se da en otras
			//coordenadas, hay que hacer una conversión
			rb.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			
		} 


	}

	void OnMouseDown () {

		isPressed = true;
		rb.isKinematic = true;
	}

	void OnMouseUp () {

		isPressed = false;
		rb.isKinematic = false;
		StartCoroutine(Release());
		StartCoroutine(DestruirYa());

	}

	public bool AgendarDestruccion ()
	{
		return destruir;
	}

	IEnumerator Release ()
	{

		yield return new WaitForSeconds(releaseTime);

		GetComponent<SpringJoint2D>().enabled = false;

	}

	IEnumerator DestruirYa ()
	{

		yield return new WaitForSeconds(tiempoVida);
		destruir = true;
		nuevoProyectil.CrearNuevoProyectil();
		Destroy(transform.parent.gameObject);

	}

}
